package salariati.repository.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {
    private EmployeeRepositoryInterface employeeRepository;

    @BeforeEach
    void setUp() {
        employeeRepository = new EmployeeMock();
    }

    @Test
    void TC1_ECP() {
        Employee newEmployee = new Employee("Avram", "Ioan", "1234567891234", DidacticFunction.ASISTENT, "2200");
        employeeRepository.addEmployee(newEmployee);
        assertTrue(employeeRepository.getEmployeeList().size() == 7);
    }

    @Test
    void TC1_BVA() {
        Employee newEmployee = new Employee("", "Ioan", "1234567891234", DidacticFunction.ASISTENT, "2200");
        employeeRepository.addEmployee(newEmployee);
        assertTrue(employeeRepository.getEmployeeList().size() == 6);
    }

    @Test
    void TC9_BVA() {
        Employee newEmployee = new Employee("Avram", "I", "1234567891234", DidacticFunction.ASISTENT, "2200");
        employeeRepository.addEmployee(newEmployee);
        assertTrue(employeeRepository.getEmployeeList().size() == 6);
    }

    @Test
    void TC20_BVA() {
        Employee newEmployee = new Employee("Avram", "Io", "1234567891234", DidacticFunction.ASISTENT, "2200");
        employeeRepository.addEmployee(newEmployee);
        assertTrue(employeeRepository.getEmployeeList().size() == 7);
    }

    @Test
    void TC21_BVA() {
        Employee newEmployee = new Employee("Avram", "Ioa", "1234567891234", DidacticFunction.ASISTENT, "2200");
        employeeRepository.addEmployee(newEmployee);
        assertTrue(employeeRepository.getEmployeeList().size() == 7);
    }

    @Test
    void TC32_BVA() {
        Employee newEmployee = new Employee("Avram", "Ioan", "123456789123", DidacticFunction.ASISTENT, "2200");
        employeeRepository.addEmployee(newEmployee);
        assertTrue(employeeRepository.getEmployeeList().size() == 6);
    }

    @Test
    void TC33_BVA() {
        Employee newEmployee = new Employee("Avram", "Ioan", "12345678912345", DidacticFunction.ASISTENT, "2200");
        employeeRepository.addEmployee(newEmployee);
        assertTrue(employeeRepository.getEmployeeList().size() == 6);
    }

    @Test
    void TC49_BVA() {
        Employee newEmployee = new Employee("Avram", "Ioan", "1234567891234", DidacticFunction.ASISTENT, "1999");
        employeeRepository.addEmployee(newEmployee);
        assertTrue(employeeRepository.getEmployeeList().size() == 6);
    }

    @Test
    void TC50_BVA() {
        Employee newEmployee = new Employee("Avram", "Ioan", "1234567891234", DidacticFunction.ASISTENT, "2000");
        employeeRepository.addEmployee(newEmployee);
        assertTrue(employeeRepository.getEmployeeList().size() == 6);
    }

    @Test
    void TC51_BVA() {
        Employee newEmployee = new Employee("Avram", "Ioan", "1234567891234", DidacticFunction.ASISTENT, "2001");
        employeeRepository.addEmployee(newEmployee);
        assertTrue(employeeRepository.getEmployeeList().size() == 7);
    }

}